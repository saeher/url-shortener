'use strict'

;(async () => {
  // load config
  require('dotenv').config()
  const debug = require('debug')('url-shortener:index')
  const error = require('debug')('url-shortener:index:error')
  const { statics } = require('./lib/config')
  // start the service
  try {
    const models = require('./lib/models')()
    require('./lib/app')(models)
    debug(statics.app.start)
  } catch (e) {
    error(e.message)
  }
})()
