const error = require('debug')('url-shortener:error')

module.exports = {
  handleErr
}

function handleErr (ctx, err, httpCode) {
  error(err.error || err)
  ctx.status = httpCode || 400
  ctx.body = {
    message: err.message || err
  }
}
