'use strict'

const router = require('koa-router')()
const { getItemCode, createItemUrl } = require('../controller/urlShortener')

router.get('/api/url/:code', getItemCode)
router.post('/api/url', createItemUrl)

module.exports = router
