'use strict'

const debug = require('debug')('url-shortener:urlShortener')
const { statics, error } = require('../config')
const helper = require('../helper')
const validUrl = require('valid-url')
const shortid = require('shortid')

module.exports = {
  getItemCode,
  createItemUrl
}

/**
 * Check the url code is create by our system
 * Redirect to the original url
 */
async function getItemCode (ctx, next) {
  debug(getItemCode.name)

  const code = ctx.checkParams('code').notEmpty().value
  if (ctx.errors) {
    return helper.handleErr(ctx, ctx.errors, statics.http.status.badRequest)
  }
  const item = await ctx.models.urlShortener.getItemCode(code)
  if (!item) {
    return helper.handleErr(ctx, error.itemNotFound, statics.http.status.badRequest)
  }

  ctx.status = statics.http.status.success
  ctx.body = {
    originalUrl: item.original_url
  }
}

/**
 * Create shortener url from original url with base url
 */
async function createItemUrl (ctx, next) {
  debug(createItemUrl.name)
  const baseUrl = ctx.checkBody('baseUrl').notEmpty().value
  const originalUrl = ctx.checkBody('originalUrl').notEmpty().value
  if (ctx.errors) {
    return helper.handleErr(ctx, ctx.errors, statics.http.status.badRequest)
  }

  // check baseUrl and originalUrl should be uri format
  if (!validUrl.isUri(baseUrl) || !validUrl.isUri(originalUrl)) {
    return helper.handleErr(ctx, error.invalidUrl, statics.http.status.badRequest)
  }

  // check duplicate original url
  const existingOriginalUrl = await ctx.models.urlShortener.getItemCodeByOriginalUrl(originalUrl)
  if (existingOriginalUrl) {
    // return existing result
    ctx.body = {
      urlCode: existingOriginalUrl.url_code,
      shortUrl: existingOriginalUrl.short_url
    }
  } else {
    // create new short url
    const urlCode = shortid.generate()
    const shortUrl = `${baseUrl}/${urlCode}`
    await ctx.models.urlShortener.createItemUrl([urlCode, shortUrl, originalUrl])

    ctx.body = {
      urlCode,
      shortUrl
    }
  }
  ctx.status = statics.http.status.success
}
