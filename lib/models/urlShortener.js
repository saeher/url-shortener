'use strict'

const debug = require('debug')('url-shortener:models:')

let query
module.exports = q => {
  query = q
  return urlShortener
}

class urlShortener {
  static getItemCode (code) {
    debug(this.getItemCode.name)
    return query(
      `
      SELECT 
        url_code,
        short_url,
        original_url
      FROM
        url_shortener
      WHERE
        url_code = ?
      `,
      code,
      results => {
        return results.pop()
      }
    )
  }

  static createItemUrl (objectData) {
    debug(this.createItemUrl.name)
    return query(
      `
      INSERT INTO 
        url_shortener
      SET
        url_code = ?,
        short_url = ?,
        original_url = ?,
        created_at = NOW(),
        updated_at = NOW()
      `,
      objectData,
      results => {
        return results
      }
    )
  }

  static getItemCodeByOriginalUrl (originalUrl) {
    debug(this.getItemCodeByOriginalUrl.name)
    return query(
      `
      SELECT 
        url_code,
        short_url
      FROM
        url_shortener
      WHERE
        original_url = ?
      `,
      originalUrl,
      results => {
        return results.pop()
      }
    )
  }
}
