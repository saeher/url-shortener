'use strict'

const debug = require('debug')('url-shortener:app')
const error = require('debug')('url-shortener:app:error')
const Koa = require('koa')
const bodyparser = require('koa-bodyparser')
const cors = require('@koa/cors')
const { env, statics } = require('./config')
const health = require('koa-heartbeat')(statics.app.health)
const router = require('./routes')

module.exports = models => {
  const app = new Koa()
  app.use(cors())
  // enable validation
  require('koa-validate')(app)
  // register models
  app.context.models = models
  // health check
  app.use(health)
  // routing
  app.use(bodyparser(statics.app.body))
  // router
  app.use(router.routes())
  app.use(router.allowedMethods())
  // return app instance
  return app
    .listen(env.port)
    .on('error', err => _handleErr(err))
}

function _handleErr (err) {
  debug(_handleErr.name)
  error(err.message)
}
