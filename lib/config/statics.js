'use strict'

module.exports = {
  app: {
    health: {
      path: '/health'
    },
    body: {
      jsonLimit: '50mb'
    }
  },
  http: {
    status: {
      success: 200,
      badRequest: 400,
      notFound: 404
    }
  }
}
