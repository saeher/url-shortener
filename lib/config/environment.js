'use strict'

module.exports = {
  nodeEnv: process.env.NODE_ENV || 'production',
  port: +process.env.PORT || 3000,
  mysql: {
    host: process.env.DATABASE_HOST,
    database: process.env.DATABASE_NAME,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PWD
  }
}
