'use strict'

module.exports = {
  sql: {
    connection: err => new Error(`SQL connection failed with error "${err.message}"`),
    query: err => new Error(`SQL query failed with error "${err.message}"`)
  },
  itemNotFound: new Error('This url can not be found'),
  invalidUrl: new Error('Base url or Original url is invalid')
}
