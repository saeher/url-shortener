# URL-Shortener

Building custom URL Shortener with Koa and Mysql

## Technologies

- ### Back end
  - [Koa.js](https://koajs.com/) Nodejs framework for building the REST Api
  - [Mysql2](https://www.npmjs.com/package/mysql2) MySQL client for Node.js

- ### Frontend end
  - [React](https://reactjs.org/) - JavaScript library for building user interfaces.
  - [React-router](https://github.com/ReactTraining/react-router)- Complete routing library for React

## Database
A changelog has historically included all database changes as project, Please run sql command under [changelog](changelog) folder. 
```bash
changelog/
  db-1.0.0.sql
  .
  .
  .
```

## Clone the project
```bash
  # clone 
  git clone git@gitlab.com:saeher/url-shortener.git

  cd url-shortener
```

## Run locally
```bash
  # create new env file, update all variables with correct values
  cp .env.dist .env && vim .env
  # run the app
  npm install
  npm run dev
```

## Note
any issue, If client app can't call to service please check
- check `REACT_APP_API_URL` config in `.env` file is correct.
- Try to call service with postman
  - [GET] /health


