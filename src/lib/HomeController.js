import { Component } from 'react'
import axios from 'axios'
import config from '../lib/config'

export default class HomeController extends Component {
  constructor (props) {
    super(props)
    this.state = {
      shortenUrl: '',
      originalUrl: '',
      baseUrl: '',
      urlCode: '',
      isEmptyValue: '',
      onError: false
    }
  }

  async createShortenerUrl () {
    if (this.originalUrl.value && this.baseUrl.value) {
      try {
        const res = await axios({
          method: 'post',
          url: `${config.apiUrl}${config.createUrl}`,
          data: {
            originalUrl: this.originalUrl.value,
            baseUrl: this.baseUrl.value
          }
        })
        this.setState({
          shortenUrl: res.data.shortUrl,
          urlCode: res.data.urlCode,
          onError: false,
          isEmptyValue: ''
        })
      } catch (err) {
        this.setState({onError: true})
      }
    } else {
      this.setState({isEmptyValue: 'home__is_empty_value'})
    }
  }

  async openShortenerUrl (urlCode) {
    try {
      const res = await axios({
        method: 'get',
        url: `${config.apiUrl}${config.createUrl}/${urlCode}`,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET'
        },
        withCredentials: false
      })
      this.setState({onError: false})
      window.open(res.data.originalUrl, '_blank')
    } catch (err) {
      this.setState({onError: true})
    }
  }

  onReset () {
    this.setState({
      shortenUrl: '',
      originalUrl: '',
      baseUrl: '',
      urlCode: '',
      isEmptyValue: '',
      onError: false
    })
  }
}
