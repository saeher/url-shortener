module.exports = {
  apiUrl: process.env.REACT_APP_API_URL,
  createUrl: '/api/url',
  openUrl: '/api/url/'
}
