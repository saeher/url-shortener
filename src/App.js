import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Layout from './components/Layout'
import Home from './components/Home'
import './App.css'

function App () {
  return (
    <BrowserRouter>
      <Layout />
      <Switch>
        <Route path='/' exact component={Home} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
