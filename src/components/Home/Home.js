import React from 'react'
import { Container, Form, Button, Alert } from 'react-bootstrap'
import HomeController from '../../lib/HomeController'

class Home extends HomeController {
  render () {
    return (
      <Container className='home__container'>
        <Form>
          <Form.Group controlId='formOriginalUrl'>
            <Form.Label>Original URL</Form.Label>
            <Form.Control
              className={this.state.isEmptyValue}
              type='text'
              placeholder='Please enter you link'
              ref={(c) => this.originalUrl = c}
              name='originalUrl'
            />
          </Form.Group>
          <Form.Group controlId='formBaseUrl'>
            <Form.Label>Base URL</Form.Label>
            <Form.Control
              className={this.state.isEmptyValue}
              type='text'
              placeholder='http://localhost'
              ref={(c) => this.baseUrl = c}
              name='baseUrl'
            />
          </Form.Group>
          <Button variant='primary' size='lg' block onClick={this.createShortenerUrl.bind(this)}>
            Submit
          </Button>
          <Button type='reset' variant='secondary' size='lg' block onClick={this.onReset.bind(this)}>
            Reset
          </Button>
        </Form>

        {/* display the shorten url */}
        {this.state.shortenUrl && (
          <div className='home__shortener_title'>
            You shorten url is: {` `}
            <button
              type='button'
              className='home__button'
              onClick={this.openShortenerUrl.bind(this, this.state.urlCode)}
            >
              {this.state.shortenUrl}
            </button>
          </div>
        )}

        {/* for case error when call service */}
        {this.state.onError && (
          <Alert key='onError' variant='danger' className='home__alert'>
            <div className='home__shortener_error'>
              <p>Some thing went wrong on service, please check service is running!</p>
            </div>
          </Alert>
        )}
      </Container>
    )
  }
}

export default Home
