CREATE TABLE `url_shortener` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url_code` varchar(100) NOT NULL DEFAULT '',
  `short_url` varchar(255) NOT NULL DEFAULT '',
  `original_url` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;